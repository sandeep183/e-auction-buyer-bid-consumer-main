package com.eauction.buyer.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class BuyerBidUpdate implements Serializable {
    private String productId;
    private String bidAmount;
    private String email;
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getBidAmount() {
		return bidAmount;
	}
	public void setBidAmount(String bidAmount) {
		this.bidAmount = bidAmount;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}