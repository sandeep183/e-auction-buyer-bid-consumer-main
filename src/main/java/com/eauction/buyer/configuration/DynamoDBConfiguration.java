package com.eauction.buyer.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.client.config.ClientOverrideConfiguration;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import java.net.URI;

@Configuration
public class DynamoDBConfiguration {

   @Bean
    public DynamoDbEnhancedClient dynamoDbEnhancedClient(){
        return  DynamoDbEnhancedClient.builder()
                .dynamoDbClient(DynamoDbClient.builder()
                        .region(Region.EU_WEST_2)
                        .build())
               .build();
   }

//    @Value("${amazon.dynamodb.endpoint}")
//    private String amazonDynamoDBEndpoint;

//    @Value("${amazon.aws.accesskey}")
//    private String amazonAWSAccessKey;
//
//    @Value("${amazon.aws.secretkey}")
//    private String amazonAWSSecretKey;

//    @Bean
//    DynamoDbClient amazonDynamoDBClient() {
//        return getDynamoDbClient();
//    }
//
//    @Bean
//    DynamoDbEnhancedClient dynamoDbEnhancedClient() {
//        return DynamoDbEnhancedClient.builder()
//                .dynamoDbClient(getDynamoDbClient()).build();
//    }
//
//    private DynamoDbClient getDynamoDbClient() {
//        ClientOverrideConfiguration.Builder overrideConfig =
//                ClientOverrideConfiguration.builder();
//
//        return DynamoDbClient.builder()
//                .overrideConfiguration(overrideConfig.build())
//                .endpointOverride(URI.create("http://localhost:8000"))
//                .region(Region.EU_WEST_2)
//                .credentialsProvider(StaticCredentialsProvider.create(amazonAWSCredentials()))
//                .build();
//    }
//
//    @Bean
//    public AwsCredentials amazonAWSCredentials() {
//        return AwsBasicCredentials.create("AKIA5LLXHX4MVJADPNP4", "7jEqEgCtm5RWAEWCyvnSLADcpKTHbMHlLqN+XKel");
//    }

}